;(function($){

    var componentMapper = {
        "grid-table" : window.kGridTable,
        "date-range" : window.kDateRange,
        "number-range" : window.kNumberRange,
        "datetime-picker" : window.kDatetimePicker,
        "multi-select" : window.kMultiSelect
    };

    function parseSettings($obj){
        var parsedSettings = {},
            blockedParameters = ["component", "component-autoload"];

        $.each($obj[0].attributes, function() {
            if(this.specified) {
                var matches = this.name.match(/^data-(.+)/);

                if(matches && blockedParameters.indexOf(matches[1]) < 0){
                    parsedSettings[matches[1]] = this.value;
                }
            }
        });

        return parsedSettings;
    }

    function getComponent($obj){
        var key = $obj.data("component");

        if(!componentMapper[key]){
            console.warn('"' + key + '" için component bulunamadı');

            return false;
        }
        else{
            return componentMapper[key];
        }
    }

    // Setup component
    $.fn.kComponent = function(){
        if(arguments[0] &&  _.isObject(arguments[0]) || arguments.length == 0){
            var initialSettings = arguments[0];

            return this.each(function(){
                var $obj = $(this),
                    data = $obj.data("ComponentData");

                if(!data){
                    // collect settings
                    var settings = $.extend({}, _.isObject(initialSettings) ? initialSettings : {}, parseSettings($obj));

                    // get component function
                    var Component = getComponent($obj);

                    if(Component){
                        // load component
                        data = new Component($obj, settings);
                        data.initialize();

                        // store component for later use
                        $obj.data("ComponentData", data);

                        // trigger associated event
                        $obj.trigger("afterComponentLoaded");
                    }
                }
                return $obj;
            });
        }
        else{
            var key = arguments[0],
                $obj = this,
                data = $obj.data("ComponentData");

            if(data){
                var params = Array.prototype.slice.call(arguments, 1);

                // @todo: burayı bi ara kontrol et, içime sinmedi, daha iyi olabilir
                if(_.isFunction(data[key])){
                    return data[key].apply($obj, params);
                }
                else if(arguments.length >= 2){
                    data[key] = params[0];
                }
                else{
                    return data[key];
                }
            }
        }
    };

    // load components on document load
    $(function(){
        if(!window._){
            console.warn('"lodash" library\'si yüklü olmalı');
            return;
        }

        var componentGroups = [ [], [], [], [], [], [] ];

        $("[data-component]").each(function(){
            var $this = $(this);

            // load the component if autoload is not false
            if([false, "false", "0"].indexOf($this.data("component-autoload")) < 0)
            {
                if($this.data("component") == "date-range"){
                    componentGroups[0].push($this);
                }
                else if($this.data("component") == "number-range"){
                    componentGroups[1].push($this);
                }
                else if($this.data("component") == "datetime-picker"){
                    componentGroups[2].push($this);
                }
                else if($this.data("component") == "multi-select"){
                    componentGroups[3].push($this);
                }
                else if($this.data("component") == "grid-table"){
                    componentGroups[4].push($this);
                }
                else{
                    componentGroups[5].push($this);
                }
            }
        });

        // load components
        for(var i= 0, j= componentGroups.length; i<j; i++){
            for(var k= 0, l= componentGroups[i].length; k<l; k++){
                componentGroups[i][k].kComponent();
            }
        }
    });
}(jQuery));