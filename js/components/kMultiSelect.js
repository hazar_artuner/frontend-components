;(function(global){
    var defaultSettings = {
        "onchange" : function(e, data){ }
    };

    function kMultiSelect($obj, settings){
        settings = $.extend({}, defaultSettings, settings);

        var events = {},
            pluginConfig,
            componentApi = null,
            $pluginRootElement,
            $checkboxes;

        function initialize()
        {
            if(!checkCompatibility()){
                return false;
            }

            $obj.addClass("not-select-filter");

            $obj.attr("multiple", "multiple");


            pluginConfig = {
                onChange : function(option, checked){

                    var event = $.Event("onChange"),
                        data = {
                            "options" : $obj.val()
                        };

                    $obj.trigger(event, data);
                    settings.onchange.apply(this, [event, data]);
                }
            };

            $obj.multiselect(pluginConfig);

            $pluginRootElement = $obj.find("+ .btn-group");
            $checkboxes = $pluginRootElement.find("input[type='checkbox']");
            $checkboxes.addClass("not-checkbox-filter");
        }

        function checkCompatibility(){
            var isCompatible = true;

            if(!$.fn.multiselect){
                isCompatible = false;
                console.warn('"multiselect" plugin\'i yüklenmeli!');
            }

            return isCompatible;
        }


        componentApi = {
            initialize  :   initialize
        };

        return componentApi;
    }

    global.kMultiSelect = kMultiSelect;
}(window));
