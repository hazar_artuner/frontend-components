;(function(global){
    var defaultSettings = {
        data : null,
        searching : true,
        pagination : true,
        exportable : true,
        ordering : true,
        'row-count' : 10,
        'min-height' : 0
    };

    function kGridTable($obj, settings){
        settings = $.extend({}, defaultSettings, settings);

        var tableApi,
            events = {},
            pluginConfig = {},
            $pluginRootElement,
            detailColumnIndexies = [];

        function initialize()
        {
            // Eğer eksik plugin varsa uyar ve uygulama akışını etkilememek için component'ı çalıştırma
            if(!checkCompatibility()){
                return false;
            }

            // Search elemanlarıni generate et ve gerekiyorsa developer'a uyarı yap. Bu kontrol
            // plugin kurulmadan önce yapılmalı çünkü sonrasında elemanların yerleri değişiyor.
            if([true, 1, "true", "1"].indexOf(settings.searching) >= 0){
                if($obj.find("> tfoot").length <= 0){
                    console.warn('"Arama" özelliğinin çalışabilmesi için lütfen tfoot > tr > th > div.filter[data-type] yapısı kurun.')
                }
            }

            // Gizlenecek veya Gösterilecek columları belirlemek için bu columnların index'lerini
            // ileriki kullanımlar için bir değişkende tutuyoruz
            $obj.find("thead [data-group='detail']").each(function(){
               detailColumnIndexies.push($(this).index());
            });

            $obj.find("tbody tr").addClass("exportable-row");

            pluginConfig = {
                "paging": [true, 1, "true", "1"].indexOf(settings.pagination) >= 0,
                "ordering": [true, 1, "true", "1"].indexOf(settings.ordering) >= 0,
                "searching": [true, 1, "true", "1"].indexOf(settings.searching) >= 0,
                "scrollX": true,
                "export" : [true, 1, "true", "1"].indexOf(settings.exportable) >= 0,
                "pageLength" : settings['row-count'],
                lengthChange : false,
                language : {
                    "decimal":        ".",
                    "emptyTable":     "Tabloda herhangi bir veri mevcut değil",
                    "info":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
                    "infoEmpty":      "Kayıt yok",
                    "infoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Sayfada _MENU_ kayıt göster",
                    "loadingRecords": "Yükleniyor...",
                    "processing":     "İşleniyor...",
                    "search":         "Ara:",
                    "zeroRecords":    "Eşleşen kayıt bulunamad",
                    "paginate": {
                        "first":      "İlk",
                        "last":       "Son",
                        "next":       "Sonraki",
                        "previous":   "Önceki"
                    },
                    "aria": {
                        "sortAscending":  ": artan sütun sıralamasını aktifleştir",
                        "sortDescending": ": azalan sütun soralamasını aktifleştir"
                    }
                },
                order : settings.data || []
            };

            if(settings.data){
                pluginConfig.data = settings.data;
            }

            // grid'in vertical scroll'unu hesaplıyoruz
            if(!pluginConfig.paging){
                pluginConfig.scrollY = calculateVerticalScroll();
            }

            if([true, 1, "true", "1"].indexOf(settings.exportable) >= 0){
                pluginConfig.dom ='Bfrtip';
                pluginConfig.buttons = [{
                    extend: 'csv',
                    text: 'Csv Export',
                    header: false,
                    className : "btn btn-primary"
                }];
            }

            if($obj.find("thead tr th[data-group='detail']").length > 0){
                pluginConfig.buttons.push({
                    text : "Detaylı Gösterim",
                    className : 'btn btn-primary active',
                    action : function(e){
                        var $button = $(e.target).closest(".btn");

                        $button.toggleClass("active");

                        events.onToggleDetailsChanged.call(this, $button);
                    }
                })
            }

            // plugin'i kur
            tableApi = $obj.DataTable(pluginConfig);

            // search aktif ise gerekli düzenlemeleri yap
            if([true, 1, "true", "1"].indexOf(settings.searching) >= 0){

                $pluginRootElement = $obj.parents(".dataTables_wrapper").eq(0);

                // DataTable plugin'inin search özelliğini extend et
                extendSearch();

                // search input'larını yukarı al
                $pluginRootElement.find(".dataTables_scrollBody").insertAfter($pluginRootElement.find(".dataTables_scrollFoot"));

                $pluginRootElement.find(".dataTables_filter").remove();

                setTimeout(function(){
                    $(window).resize();
                }, 0);
            }
        }

        function calculateVerticalScroll(){
            var windowHeight = $(window).height(),
                dataTableHeight = $obj.height();

            if (windowHeight - dataTableHeight < 500) {
                dataTableHeight = windowHeight - 500;
            }

            if(settings["min-height"]){
                var minHeight = parseInt(settings["min-height"], 10);

                dataTableHeight = Math.max(dataTableHeight, minHeight);
            }

            return dataTableHeight;
        }

        function setData(data){
            tableApi.clear();
            tableApi.rows.add(data);
            tableApi.draw();
        }

        function checkCompatibility(){
            var isCompatible = true;

            if(!global._){
                isCompatible = false;
                console.warn('"lodash" plugin\'i yüklenmeli!');
            }

            return isCompatible;
        }

        function extendSearch(){
            extendDateRangeSearch();
            extendNumberRangeSearch();
            extendDatetimePickerSearch();
            extendMultiSelectSearch();

            // bind search events
            tableApi.columns().every( function () {
                var that = this;

                $( 'input[type=text]:not(.not-text-filter), input[type=checkbox]:not(.not-checkbox-filter), select:not(.not-select-filter)', this.footer() ).on( 'keyup change', function (e) {
                    if(e.keyCode == 13 || e.type == "change"){
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    }
                } );
            } );

            $pluginRootElement.find("[data-component='number-range'] input").on("keyup", function(e){
                if(e.keyCode == 13){
                    tableApi.draw();
                }
            });

            $pluginRootElement.find("[data-component='date-range'] input").on("change", function(){
                tableApi.draw();
            });

            $pluginRootElement.find("[data-component='datetime-picker'] input").on("change", function(){
                tableApi.draw();
            });

            $pluginRootElement.find("[data-component='multi-select']").on("onChange", function(e, data){
                tableApi.draw();
            });
        }

        function extendNumberRangeSearch(){
            var $numberRanges = $pluginRootElement.find(".dataTables_scrollFoot tfoot [data-component='number-range']");

            $.fn.dataTable.ext.search.push(
                (function(){
                    // kullanacağın değişkenleri cache'le
                    var $columns = [];

                    $numberRanges.each(function(){
                        var $this = $(this);

                        $columns.push({
                            $obj : $this,
                            columnIndex : $this.parent().index()
                        })
                    });

                    return function(settings, data, dataIndex){
                        var isMatching = true;

                        // Search aksiyonunun doğru çalışabilmesi için aynı tipteki tüm column'ları search
                        // etmen gerekiyor sadece değişiklik yaptığın column'u search edersen doğru çalışmaz.
                        $.each($columns, function(i, obj){
                            var min = parseFloat( obj.$obj.kComponent("getMin")),
                                max = parseFloat( obj.$obj.kComponent("getMax")),
                                value = parseFloat( data[obj.columnIndex] ) || 0;

                            if(!_.isNaN(min) && _.isNaN(max) && (value >= min)){
                                isMatching = true;
                            }
                            else if(_.isNaN(min) && !_.isNaN(max) && (value <= max)){
                                isMatching = true;
                            }
                            else if(_.isNaN(min) && _.isNaN(max)){
                                isMatching = true;
                            }
                            else {
                                isMatching = (value >= min && value <= max);
                            }

                            if(!isMatching){
                                return false;
                            }
                        });

                        return isMatching;
                    };
                }())
            );
        }

        function extendDateRangeSearch(){
            var $dateRanges = $pluginRootElement.find(".dataTables_scrollFoot tfoot [data-component='date-range']");

            $.fn.dataTable.ext.search.push(
                (function(){
                    // kullanacağın değişkenleri cache'le
                    var $columns = [];

                    $dateRanges.each(function(){
                        var $obj = $(this),
                            $input = $obj.find("input");

                        $columns.push({
                            $obj : $obj,
                            $input : $input,
                            valueFormat : $obj.data("format"),
                            columnIndex : $obj.parent().index()
                        });
                    });

                    return function(settings, data){
                        var isMatching = true;

                        // Search aksiyonunun doğru çalışabilmesi için aynı tipteki tüm column'ları search
                        // etmen gerekiyor sadece değişiklik yaptığın column'u search edersen doğru çalışmaz.
                        $.each($columns, function(i, obj){

                            if($.trim(obj.$input.val()) != ""){

                                var startDate = obj.$obj.kComponent("getStartDate"),
                                    endDate = obj.$obj.kComponent("getEndDate"),
                                    value = moment(data[obj.columnIndex], obj.valueFormat),
                                    rangeType = obj.$obj.kComponent("getSetting", "date-type");


                                if(rangeType == "start-date"){
                                    isMatching = value.unix() >= startDate.unix();
                                }
                                else if(rangeType == "end-date"){
                                    isMatching = value.unix() <= endDate.unix();
                                }
                                else{
                                    if(startDate.isValid() && endDate.isValid()){
                                        isMatching = (value.unix() >= startDate.unix() && value.unix() <= endDate.unix());
                                    }
                                }

                                if(!isMatching){
                                    return false;
                                }
                            }
                        });

                        return isMatching;
                    };
                }())
            );
        }

        function extendDatetimePickerSearch(){
            var $dates = $pluginRootElement.find(".dataTables_scrollFoot tfoot [data-component='datetime-picker']");

            $.fn.dataTable.ext.search.push(
                (function(){
                    // kullanacağın değişkenleri cache'le
                    var $columns = [];

                    $dates.each(function(){
                        var $obj = $(this),
                            $input = $obj.find("input");

                        $columns.push({
                            $obj : $obj,
                            $input : $input,
                            valueFormat : $obj.data("format"),
                            columnIndex : $obj.parent().index()
                        });
                    });

                    return function(settings, data){
                        var isMatching = true;

                        // Search aksiyonunun doğru çalışabilmesi için aynı tipteki tüm column'ları search
                        // etmen gerekiyor sadece değişiklik yaptığın column'u search edersen doğru çalışmaz.
                        $.each($columns, function(i, obj){

                            if($.trim(obj.$input.val()) != ""){
                                var date = obj.$obj.kComponent("getDatetime"),
                                    value = moment(data[obj.columnIndex], obj.valueFormat);

                                if(date.isValid() && value.isValid()){
                                    isMatching = value.unix() == date.unix();
                                }

                                if(!isMatching){
                                    return false;
                                }
                            }
                        });

                        return isMatching;
                    };
                }())
            );
        }

        function extendMultiSelectSearch(){
            var $multiSelects = $pluginRootElement.find(".dataTables_scrollFoot tfoot [data-component='multi-select']");

            $.fn.dataTable.ext.search.push(
                (function(){
                    // kullanacağın değişkenleri cache'le
                    var $columns = [];

                    $multiSelects.each(function(){
                        var $obj = $(this);

                        $columns.push({
                            $obj : $obj,
                            separator : $obj.data("separator") || ",",
                            columnIndex : $obj.parent().index()
                        })
                    });

                    return function(settings, data, dataIndex){
                        var isMatching = true;

                        // Search aksiyonunun doğru çalışabilmesi için aynı tipteki tüm column'ları search
                        // etmen gerekiyor sadece değişiklik yaptığın column'u search edersen doğru çalışmaz.
                        $.each($columns, function(i, obj){
                            var values = data[obj.columnIndex].split(obj.separator),
                                selectedItems = obj.$obj.val();

                            // tum elemanlara trim uygula
                            values = $.map(values, $.trim);

                            if(selectedItems)
                            {
                                for(var i= 0, l=selectedItems.length; i<l; i++)
                                {
                                    if(selectedItems[i]){
                                        if(values.indexOf(selectedItems[i]) < 0)
                                        {
                                            isMatching = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        });

                        return isMatching;
                    };
                }())
            );
        }

        events.onToggleDetailsChanged = function($button){
            var isVisible = $button.hasClass("active");

            for (var i = 0, l = detailColumnIndexies.length; i<l; i++) {
                var column = tableApi.column(detailColumnIndexies[i]);

                column.visible(isVisible);
            }
        };

        return {
            initialize  :   initialize,
            setData : setData
        };
    }

    global.kGridTable = kGridTable;
}(window));
