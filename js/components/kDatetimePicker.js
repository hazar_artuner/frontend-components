;(function(global){
    var defaultSettings = {
        format: 'MM/DD/YYYY HH:mm'
    };

    function kDatetimePicker($obj, settings){
        settings = $.extend({}, defaultSettings, settings);

        var html,
            $input,
            $reset,
            events = {},
            pluginApi = null;


        function initialize()
        {
            if(!checkCompatibility()){
                return false;
            }

            var isEmptyOnLoad = $.trim($obj.val()).length <= 0;

            html  = "<span class='btn-reset pull-right'><i class='glyphicon glyphicon-remove'></i></span>";
            html += "<div class='fluid-content input-wrapper'>";
            html += "<input type='text' class='not-text-filter form-control' placeholder='Tarih' /></div>";

            $obj.addClass("datetime-picker-wrapper")
                .html(html);

            $reset = $obj.find(".btn-reset");
            $input = $obj.find(".input-wrapper input");

            if(settings.name){
                $input.prop("name", settings.name);
            }

            $input.daterangepicker({
                timePicker24Hour: true,
                singleDatePicker: true,
                timePicker: true,
                locale : {
                    applyLabel: 'Kullan',
                    cancelLabel: 'İptal',
                    "daysOfWeek": [
                        "Pz",
                        "Pt",
                        "Sa",
                        "Çr",
                        "Pr",
                        "Cm",
                        "Ct"
                    ],
                    "monthNames": [
                        "Ocak",
                        "Şubat",
                        "Mart",
                        "Nisan",
                        "Mayıs",
                        "Haziran",
                        "Temmuz",
                        "Ağustos",
                        "Eylül",
                        "Ekim",
                        "Kasım",
                        "Aralık"
                    ],
                    format : settings.format
                }
            });

            pluginApi = $input.data("daterangepicker");

            // ilk anda filtre'yi sıfırlamamız gerekiyor
            if(isEmptyOnLoad){
                $input.val("");
            }

            bindEvents();
        }

        function bindEvents(){
            $reset.on("click", events.onResetButtonClicked);
        }

        function getDatetime(){
            if(!pluginApi){
                throw Error("önce plugin'i initialize edin!");
            }
            else{
                return pluginApi.startDate;
            }
        }

        function setDatetime(date){
            if(!pluginApi){
                throw Error("önce plugin'i initialize edin!");
            }
            else{
                return pluginApi.setStartDate(date);
            }
        }

        function checkCompatibility(){
            var isCompatible = true;

            if(!$.fn.daterangepicker){
                isCompatible = false;
                console.warn('"daterangepicker" plugin\'i yüklenmeli!');
            }

            return isCompatible;
        }

        events.onResetButtonClicked = function(){
            $input.val("").trigger("change");
        };

        return {
            initialize  :   initialize,
            getDatetime : getDatetime,
            setDatetime : setDatetime
        };
    }

    global.kDatetimePicker = kDatetimePicker;
}(window));
