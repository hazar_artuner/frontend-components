;(function(global){
    var defaultSettings = {
        "date-type" : "range",
        "format" : "YYYY-MM-DD"
    };

    function kDateRange($obj, settings){
        settings = $.extend({}, defaultSettings, settings);

        var html,
            $input,
            $reset,
            events = {},
            pluginApi = null;


        function initialize()
        {
            if(!checkCompatibility()){
                return false;
            }

            var isEmptyOnLoad = $.trim($obj.val()).length <= 0,
                pluginConfig = {
                    alwaysShowCalendars : true,
                    locale : {
                        applyLabel: 'Kullan',
                        cancelLabel: 'İptal',
                        customRangeLabel: 'Özel',
                        "daysOfWeek": [
                            "Pz",
                            "Pt",
                            "Sa",
                            "Çr",
                            "Pr",
                            "Cm",
                            "Ct"
                        ],
                        "monthNames": [
                            "Ocak",
                            "Şubat",
                            "Mart",
                            "Nisan",
                            "Mayıs",
                            "Haziran",
                            "Temmuz",
                            "Ağustos",
                            "Eylül",
                            "Ekim",
                            "Kasım",
                            "Aralık"
                        ],
                        format : settings.format
                    },
                    ranges: {
                        'Bugün': [moment(), moment()],
                        'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Son 7 Gün': [moment().subtract(6, 'days'), moment()],
                        'Son 30 Gün': [moment().subtract(29, 'days'), moment()],
                        'Bu Ay': [moment().startOf('month'), moment().endOf('month')],
                        'Geçen Ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                };

            html  = "<span class='btn-reset pull-right'><i class='glyphicon glyphicon-remove'></i></span>";
            html += "<div class='fluid-content input-wrapper'>";
            html += "<input type='text' class='not-text-filter form-control' placeholder='Tarih Aralığı' /></div>";

            $obj.addClass("date-range-wrapper")
                .html(html);

            $reset = $obj.find(".btn-reset");
            $input = $obj.find(".input-wrapper input");

            if(settings.name){
                $input.prop("name", settings.name);
            }

            if(settings["date-type"] != "range"){
                pluginConfig.singleDatePicker = true;
            }

            $input.daterangepicker(pluginConfig);

            pluginApi = $input.data("daterangepicker");

            // ilk anda filtre'yi sıfırlamamız gerekiyor
            if(isEmptyOnLoad){
                $input.val("");
            }

            bindEvents();
        }

        function bindEvents(){
            $reset.on("click", events.onResetButtonClicked);
        }

        function getStartDate(){
            if(!pluginApi){
                throw Error("önce plugin'i initialize edin!");
            }
            else{
                return pluginApi.startDate;
            }
        }

        function getEndDate(){
            if(!pluginApi){
                throw Error("önce plugin'i initialize edin!");
            }
            else{
                return pluginApi.endDate;
            }
        }

        function setStartDate(date){
            if(!pluginApi){
                throw Error("önce plugin'i initialize edin!");
            }
            else{
                return pluginApi.setStartDate(date);
            }
        }

        function setEndDate(date){
            if(!pluginApi){
                throw Error("önce plugin'i initialize edin!");
            }
            else{
                return pluginApi.setEndDate(date);
            }
        }

        function getSetting(name){
            return settings[name];
        }

        function checkCompatibility(){
            var isCompatible = true;

            if(!$.fn.daterangepicker){
                isCompatible = false;
                console.warn('"daterangepicker" plugin\'i yüklenmeli!');
            }

            return isCompatible;
        }

        events.onResetButtonClicked = function(){
            $input.val("").trigger("change");
        };

        return {
            initialize  :   initialize,
            getStartDate : getStartDate,
            getEndDate  : getEndDate,
            setStartDate : setStartDate,
            setEndDate : setEndDate,
            getSetting : getSetting
        };
    }

    global.kDateRange = kDateRange;
}(window));
