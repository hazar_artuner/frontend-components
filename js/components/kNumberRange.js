;(function(global){
    var defaultSettings = {
        minName : "min",
        maxName : "max",
        onChange : function(){ }
    };

    function kNumberRange($obj, settings){
        settings = $.extend({}, defaultSettings, settings);

        var html,
            $min,
            $max,
            events = {},
            callbacks = {},
            componentApi = null;


        function initialize()
        {
            if(!checkCompatibility()){
                return false;
            }

            html  = "<input type='text' class='number-range-min not-text-filter form-control' placeholder='Min' />";
            html += "<input type='text' class='number-range-max not-text-filter form-control' placeholder='Max' />";

            $obj.addClass("number-range-wrapper")
                .html(html);

            $min = $obj.find(".number-range-min");
            $max = $obj.find(".number-range-max");

            $min.inputmask('Regex', {regex: "^[0-9]{1,6}(\\.\\d{1,2})?$"});
            $max.inputmask('Regex', {regex: "^[0-9]{1,6}(\\.\\d{1,2})?$"});

            bindEvents();
        }

        function bindEvents(){
            $min.on("change keyup", events.onRangeChange);
            $max.on("change keyup", events.onRangeChange);
        }

        function checkCompatibility(){
            var isCompatible = true;

            if(!$.fn.inputmask){
                isCompatible = false;
                console.warn('"inputmask" plugin\'i yüklenmeli!');
            }

            return isCompatible;
        }

        function getMin(){
            return parseFloat($min.val()) || 0;
        }

        function getMax(){
            return parseFloat($max.val());
        }

        events.onRangeChange = function(){
            var min = getMin(),
                max = getMax();

            if(_.isFunction(settings.onChange)){
                settings.onChange.call($obj, min, max);
            }

            $obj.trigger("onChange");
        };

        componentApi = {
            initialize  :   initialize,
            getMin      :   getMin,
            getMax      :   getMax
        };

        return componentApi;
    }

    global.kNumberRange = kNumberRange;
}(window));
